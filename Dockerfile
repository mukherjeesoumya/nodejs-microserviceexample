FROM node:8

#cloning GIT NodeJs Project
RUN git clone https://mukherjeesoumya@bitbucket.org/mukherjeesoumya/nodejs-microserviceexample.git

#WorkDir
ADD . /nodejs-microserviceexample
WORKDIR /nodejs-microserviceexample

#Exposing Port
EXPOSE 3000

#Run NPM dependency
RUN npm install

#Installing Packages
RUN npm install request

#Run the NodeJs project
#RUN node app.js