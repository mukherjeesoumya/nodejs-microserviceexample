
/**
 * Module dependencies.
 */

var express = require('express');
var bodyparser = require("body-parser");
var path = require('path');
var http = require('http');
var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyparser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyparser.urlencoded({ extended: true }));


var auth = require("./routes/index.js");
app.get("/", auth);

var auth = express.Router();
require('./routes/cart.js')(auth);
app.post("/postvegies", auth);
app.get("/getproductlist",auth);

var productdisplay = express.Router();
require('./routes/productdisplay.js')(productdisplay);
app.get('/getproduct',productdisplay);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
