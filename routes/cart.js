

global.veggieObject={}

module.exports = function(auth){

	//This will add vegies into the global list
	auth.post("/postvegies", function(req, res) {
		var name = req.body.name;
		var qty = req.body.qty;
		
		veggieObject[name] = qty;
		console.log(veggieObject);

		res.render("index", { title: "MicroServices" });
	});
	
	auth.get('/getproductlist',function(req,res){
			
			console.log("Calling Product List");
			res.send(global.veggieObject);
	});

};